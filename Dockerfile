FROM eclipse-temurin:21-jre-alpine
COPY target/quarkus-app/ /usr/src/
WORKDIR /usr/src/
CMD java -Xmx64m -jar quarkus-run.jar
