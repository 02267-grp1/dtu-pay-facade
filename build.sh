#!/bin/bash
set -e

# Define the service name
Service="facade-service"

echo "Building Maven project for
$Service..."
mvn clean package

echo "Building Docker image for $Service..."

docker build . -t $Service 
echo "Docker image for $Service built successfully."
