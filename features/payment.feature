Feature: REST

  Scenario: Payment REST endpoint
    When a payment request for 10 kr to merchant id "x" for customer token "x"
    Then an event "RequestPayment" is sent
    And a payment is successful
  