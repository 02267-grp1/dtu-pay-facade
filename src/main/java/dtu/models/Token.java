package dtu.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
    @author s153277
 */
@Data
@AllArgsConstructor
public class Token implements Serializable {
    private String customerId;
    private String uuid;
}
