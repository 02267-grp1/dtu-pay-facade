package dtu.models;

import lombok.Data;

import java.util.UUID;
/**
    @author s194626
 */
@Data
public class Customer {
    String firstName, lastName, cpr, bankAccountId;
}
