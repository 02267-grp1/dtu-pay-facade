package dtu.restadapter;

import messaging.implementations.RabbitMqQueue;
/**
    @author s223142
*/
public class TokenServiceFactory {
    static TokenService service = null;

    public synchronized TokenService getService() {
        if (service != null) {
            return service;
        }

        var messageQueue = new RabbitMqQueue("rabbitmq");
        service = new TokenService(messageQueue);
        return service;
    }
}
