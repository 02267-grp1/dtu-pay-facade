package dtu.restadapter;

import com.google.gson.reflect.TypeToken;
import dtu.models.Token;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class TokenService {

    private MessageQueue queue;

    private CompletableFuture<List<Token>> customerTokenResult;

    public TokenService(MessageQueue queue) {
        this.queue = queue;
        this.queue.addHandler("TokensProvided", this::handleCustomerTokenResult);
    }
    /**
        @author s153277
    */
    public void handleCustomerTokenResult(Event event) {
        List<Token> tokens = event.getArgument(0, new TypeToken<List<Token>>(){}.getType());
        customerTokenResult.complete(tokens);
    }
    /**
        @author s153277
    */
    public List<Token> getTokens(String customerId) {
        customerTokenResult = new CompletableFuture<>();

        Event event = new Event("TokensRequested", new Object[]{customerId});
        queue.publish(event);

        return customerTokenResult.join();
    }
}
