package dtu.restadapter;

import dtu.models.Merchant;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.UUID;

@Path("/merchant")
public class MerchantResource {

    MerchantService merchantService = new MerchantServiceFactory().getService();
    /**
        @author s213555
    */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public UUID createMerchant(Merchant merchant) {
        return merchantService.createMerchant(merchant);
    }
    /**
        @author s223142
    */
    @DELETE
    @Path("/{merchantId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteMerchant(@PathParam("merchantId") String merchantId) {
        String result = merchantService.deregisterMerchant(merchantId);
        return Response.ok(result).build();
    }

}
