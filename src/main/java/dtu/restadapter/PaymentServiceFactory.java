package dtu.restadapter;

import messaging.implementations.RabbitMqQueue;
/**
    @author s222994
*/
public class PaymentServiceFactory {
    static PaymentService service = null;

    public synchronized PaymentService getService() {
        if (service != null) {
            return service;
        }

        var messageQueue = new RabbitMqQueue("rabbitmq");
        service = new PaymentService(messageQueue);
        return service;
    }
}
