package dtu.restadapter;

import messaging.implementations.RabbitMqQueue;
/**
    @author s194626
*/
public class CustomerServiceFactory {

    private CustomerService service;

    public synchronized CustomerService getCustomerService(){
        if (service == null) {
            var mq = new RabbitMqQueue("rabbitmq");
            service = new CustomerService(mq);
        }

        return service;
    }
}
