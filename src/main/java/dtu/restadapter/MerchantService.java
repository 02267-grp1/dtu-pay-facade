package dtu.restadapter;

import dtu.models.Merchant;
import messaging.Event;
import messaging.MessageQueue;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class MerchantService {

    private MessageQueue queue;

    private CompletableFuture<UUID> merchantCreationResult;
    private CompletableFuture<String> merchantDeletionResult;

    public MerchantService(MessageQueue queue) {
        this.queue = queue;
        this.queue.addHandler("MerchantRegistered", this::handleMerchantCreationResult);
        this.queue.addHandler("MerchantDeregistrationCompleted", this::MerchantDeregistrationResult);
    }
    /**
        @author s213555
    */
    public void handleMerchantCreationResult(Event event) {
        UUID merchantAccountId = event.getArgument(0, UUID.class);
        merchantCreationResult.complete(merchantAccountId);
    }
    /**
        @author s223142
    */
    public void MerchantDeregistrationResult(Event event) {
        String result = event.getArgument(0, String.class);
        merchantDeletionResult.complete(result);
    }
    /**
        @author s213555
    */
    public UUID createMerchant(Merchant merchant) {
        merchantCreationResult = new CompletableFuture<>();

        Event event = new Event("MerchantRegistrationRequested", new Object[]{merchant});
        queue.publish(event);

        return merchantCreationResult.join();
    }
    /**
        @author s223142
    */
    public String deregisterMerchant(String merchantId) {
        merchantDeletionResult = new CompletableFuture<>();

        Event event = new Event("MerchantDeregisterRequested", new Object[]{merchantId});
        queue.publish(event);

        return merchantDeletionResult.join();
    }
}
