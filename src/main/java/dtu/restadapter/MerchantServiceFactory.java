package dtu.restadapter;

import messaging.implementations.RabbitMqQueue;
/**
    @author s213555
*/
public class MerchantServiceFactory {
    static MerchantService service = null;

    public synchronized MerchantService getService() {
        if (service != null) {
            return service;
        }

        var messageQueue = new RabbitMqQueue("rabbitmq");
        service = new MerchantService(messageQueue);
        return service;
    }
}
