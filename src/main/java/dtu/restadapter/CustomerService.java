package dtu.restadapter;

import dtu.models.Customer;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import messaging.Event;
import messaging.MessageQueue;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class CustomerService {

    private MessageQueue messageQueue;
    private CompletableFuture<UUID> registrationResult;
    private CompletableFuture<String> deregistrationResult;

    public CustomerService(MessageQueue messageQueue){
        this.messageQueue = messageQueue;
        this.messageQueue.addHandler("CustomerRegistered", this::handleCustomerRegistrationResult);
        this.messageQueue.addHandler("CustomerDeregistrationCompleted", this::handleCustomerDeregistrationResult);
    }
    /**
        @author s194626
    */
    private void handleCustomerRegistrationResult(Event event) {
        UUID result = event.getArgument(0, UUID.class);
        registrationResult.complete(result);
    }
    /**
        @author s202082
    */
    private void handleCustomerDeregistrationResult(Event event) {
        String result = event.getArgument(0, String.class);
        deregistrationResult.complete(result);
    }
    /**
        @author s194626
    */
    public UUID registerCustomer(Customer customer){

        //TO REMOVE
        System.out.println("registering \n" + customer);



        registrationResult = new CompletableFuture<>();

        Event event = new Event("RequestCustomerRegistration", new Object[]{customer});
        messageQueue.publish(event);

        return registrationResult.join();
    }
    /**
        @author s202082
    */
    public String deregisterCustomer(String customerId) {

        //TO REMOVE
        System.out.println("deregistering \n" + customerId);



       deregistrationResult = new CompletableFuture<>();

        Event event = new Event("CustomerDeregistrationRequested", new Object[]{customerId});
        messageQueue.publish(event);

        return deregistrationResult.join();
    }
}
