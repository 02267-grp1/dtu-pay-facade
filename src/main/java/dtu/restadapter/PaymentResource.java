package dtu.restadapter;

import dtu.models.Payment;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/payment")
public class PaymentResource {

    PaymentService paymentService = new PaymentServiceFactory().getService();
    /**
        @author s222994
    */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String payment(Payment payment) {
        return paymentService.payment(payment);
    }

}
