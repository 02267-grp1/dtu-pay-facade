package dtu.restadapter;

import dtu.models.Customer;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.UUID;

@Path("/customer")
public class CustomerResource {

    private final CustomerService service = new CustomerServiceFactory().getCustomerService();
    /**
        @author s194626
    */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public UUID registerCustomer(Customer customer){
        return service.registerCustomer(customer);
    }
    /**
        @author s202082
    */
    @DELETE
    @Path("/{customerId}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deregisterCustomer(@PathParam("customerId")String customerId){
        String result = service.deregisterCustomer(customerId);
        return Response.ok(customerId).build();
    }
}
