package dtu.restadapter;

import dtu.models.Token;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

@Path("/tokens")
public class TokenResource {

    TokenService tokenService = new TokenServiceFactory().getService();
    /**
        @author s153277
    */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Token> getTokens(@QueryParam("customerId") String customerId) {
        return tokenService.getTokens(customerId);
    }

}
