package dtu.restadapter;

import dtu.models.Payment;
import messaging.Event;
import messaging.MessageQueue;

import java.util.concurrent.CompletableFuture;

public class PaymentService {

    private MessageQueue queue;

    private CompletableFuture<String> paymentResult;

    public PaymentService(MessageQueue queue) {
        this.queue = queue;
        this.queue.addHandler("PaymentResult", this::handlePaymentResult);
    }
    /**
        @author s222994
    */
    public void handlePaymentResult(Event event) {
        String result = event.getArgument(0, String.class);
        paymentResult.complete(result);
    }
    /**
        @author s222994
    */
    public String payment(Payment payment) {
        paymentResult = new CompletableFuture<>();

        Event event = new Event("RequestPayment", new Object[]{payment});
        queue.publish(event);

        return paymentResult.join();
    }
}
