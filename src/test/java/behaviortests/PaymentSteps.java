package behaviortests;

import dtu.models.Payment;
import dtu.restadapter.PaymentService;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class PaymentSteps {
    private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();

    private MessageQueue queue = new MessageQueue() {
        @Override
        public void publish(Event event) {
            publishedEvent.complete(event);
        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }
    };

    PaymentService paymentService = new PaymentService(queue);

    private CompletableFuture<String> paymentResult = new CompletableFuture<>();

    private Event expectedEvent;

    @When("a payment request for {int} kr to merchant id {string} for customer token {string}")
    public void aPaymentRequestForKrToMerchantIdForCustomerToken(int amount, String merchantId, String customerToken) {
        expectedEvent = new Event("RequestPayment", new Object[] { new Payment(amount, customerToken, merchantId) });
        new Thread(() -> {
            var result = paymentService.payment(new Payment(amount, customerToken, merchantId));
            paymentResult.complete(result);
        }).start();
    }

    @Then("an event {string} is sent")
    public void anEventIsSent(String event) {
        // Simulate payment successful
        paymentService.handlePaymentResult(new Event("PaymentResult", new Object[]{"Payment successful"}));
        Assert.assertEquals(expectedEvent, publishedEvent.join());
    }

    @Then("a payment is successful")
    public void aPaymentIsSuccessful() {
        String expected = "Payment successful";
        Assert.assertEquals(expected, paymentResult.join());
    }

}
